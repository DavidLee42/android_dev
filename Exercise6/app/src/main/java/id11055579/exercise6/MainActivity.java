package id11055579.exercise6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import id11055579.exercise6.models.FriendData;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private FriendDatabaseHelper mFriendsDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFriendsDb = new FriendDatabaseHelper(this);

        Button addFriendButton = (Button) findViewById(R.id.main_add_friend_button);
        addFriendButton.setOnClickListener(this);

        Button listAllFriendsButton = (Button) findViewById(R.id.main_list_all_friends_button);
        listAllFriendsButton.setOnClickListener(this);

        Button searchFriendsButton = (Button) findViewById(R.id.main_search_friends_button);
        searchFriendsButton.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    /**
     * Handles the buttons in a switch statement
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.main_add_friend_button):
                Intent intent = new Intent(MainActivity.this, AddFriendActivity.class);
                startActivityForResult(intent, Constants.REQUEST_CODE);

                break;
            case (R.id.main_list_all_friends_button):
                Intent listAllIntent = new Intent(MainActivity.this, ListFriendsActivity.class);
                listAllIntent.putExtra(Constants.FRIEND_TO_FIND, Constants.ALL);
                startActivity(listAllIntent);
                break;
            case (R.id.main_search_friends_button):
                Intent findFriendIntent = new Intent(MainActivity.this, ListFriendsActivity.class);
                findFriendIntent.putExtra(Constants.FRIEND_TO_FIND, ((EditText)findViewById(R.id.main_name_edittext)).getText().toString());
                startActivity(findFriendIntent);
                break;
            default:
                //Log.e(Constants.TAG, getString(R.string.unknown_error));
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_CODE)
        {
            if(resultCode == RESULT_OK)
            {
                Calendar calendar = Calendar.getInstance();


                FriendData friend = new FriendData(0,
                        data.getStringExtra(Constants.NAME),
                        data.getStringExtra(Constants.OCCUPATION),
                        data.getStringExtra(Constants.CITY),
                        calendar.getTimeInMillis());
                mFriendsDb.addFriend(friend);
                Toast.makeText(getApplicationContext(), getString(R.string.added) + data.getStringExtra(Constants.NAME) + getString(R.string.to_database), Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), R.string.get_friend_failed, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
