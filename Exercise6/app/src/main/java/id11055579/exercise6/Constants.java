package id11055579.exercise6;

/**
 * Created by David on 8/09/2015.
 */
public class Constants {


    public static final String FRIEND_TO_FIND = "friendToFind";
    public static final String NAME = "name";
    public static final String OCCUPATION = "occupation";
    public static final String CITY = "city";
    public static final int REQUEST_CODE = 2;
    public static final String ALL = "all";
    public static final String FRIEND_SINCE = "friendsince";
    public static final String DATE_FORMAT = "dateFormat";
    public static final String DATE_FORMAT_DEFAULT = "dd/MM/yyyy";
}
