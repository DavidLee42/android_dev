package id11055579.exercise6;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import id11055579.exercise6.R;

/**
 * Created by David on 8/09/2015.
 */
public class AddFriendActivity extends AppCompatActivity implements View.OnClickListener{


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        Button addFriendButton = (Button) findViewById(R.id.cancel_add_btn);
        addFriendButton.setOnClickListener(this);

        Button listAllFriendsButton = (Button) findViewById(R.id.add_friend_btn);
        listAllFriendsButton.setOnClickListener(this);
    }

    /**
     * Handles the buttons in a switch statement
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.cancel_add_btn):
                finish();
                break;
            case (R.id.add_friend_btn):

                Intent intentMessage = new Intent();
                intentMessage.putExtra("name", ((EditText)findViewById(R.id.name_et)).getText().toString());
                intentMessage.putExtra("occupation", ((EditText)findViewById(R.id.occupation_et)).getText().toString());
                intentMessage.putExtra("city", ((EditText)findViewById(R.id.city_et)).getText().toString());

                setResult(RESULT_OK, intentMessage);
                finish();
                break;
            default:
                //Log.e(Constants.TAG, getString(R.string.unknown_error));
                break;
        }
    }

}
