package id11055579.exercise6;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;

import java.text.DateFormat;

/**
 * Created by David Lee on 1/09/2015.
 */
public class SettingsActivity extends PreferenceActivity

{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);

         final ListPreference dateFormatPreference = (ListPreference) findPreference("dateFormat");

        dateFormatPreference.setSummary(dateFormatPreference.getEntry());
        dateFormatPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {

            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {


                dateFormatPreference.setSummary(getResources().getStringArray(R.array.listArray)[Integer.parseInt(newValue.toString())-1]);
                return true;
            }
        });

    }
}
