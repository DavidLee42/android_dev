package id11055579.exercise6.models;

/**
 * Created by David on 7/09/2015.
 */
public class FriendData {

    private int mId;
    private String mName;
    private String mOccupation;
    private String mCity;
    private Long mFriendSince;


    public FriendData(int id, String name, String occupation, String city, Long friendSince) {
        this.mId = id;
        this.mName = name;
        this.mOccupation = occupation;
        this.mCity = city;
        this.mFriendSince = friendSince;
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public Long getFriendSince() {
        return mFriendSince;
    }

    public void setFriendSince(Long mFriendSince) {
        this.mFriendSince = mFriendSince;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getOccupation() {
        return mOccupation;
    }

    public void setOccupation(String mOccupation) {
        this.mOccupation = mOccupation;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }
}
