package id11055579.exercise6;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import id11055579.exercise6.models.FriendData;

/**
 * Created by David on 8/09/2015.
 */
public class FriendAdaptor extends CursorAdapter {


    private Context mContext;
    public FriendAdaptor(Context context, Cursor cursor, int flags) {
        super(context,cursor,0);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.friend_list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView tvName = (TextView) view.findViewById(R.id.list_name_tv);
        TextView tvOccupation = (TextView) view.findViewById(R.id.list_occupation_tv);
        TextView tvCity = (TextView) view.findViewById(R.id.list_city_tv);
        TextView tvFriendSince = (TextView) view.findViewById(R.id.list_friend_since_tv);


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        tvName.setText(cursor.getString(cursor.getColumnIndexOrThrow(Constants.NAME)));
        tvOccupation.setText(cursor.getString(cursor.getColumnIndexOrThrow(Constants.OCCUPATION)));
        tvCity.setText(cursor.getString(cursor.getColumnIndexOrThrow(Constants.CITY)));
        String myDateFormat = mContext.getResources().getStringArray(R.array.listArray)[Integer.parseInt(preferences.getString(Constants.DATE_FORMAT, "1"))-1];
        preferences.getString(Constants.DATE_FORMAT, Constants.DATE_FORMAT_DEFAULT);
        tvFriendSince.setText(DateFormat.format(myDateFormat, new Date(cursor.getLong(cursor.getColumnIndexOrThrow(Constants.FRIEND_SINCE)))).toString());

    }
}