package id11055579.exercise6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import id11055579.exercise6.models.FriendData;

/**
 * Created by David Lee on 1/09/2015.
 */
public class FriendDatabaseHelper extends SQLiteOpenHelper {


    public static final String TABLE_FRIENDS = "friends";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_OCCUPATION = "occupation";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_FRIENDSINCE = "friendsince";

    private static final String[] COLUMNS = {COLUMN_ID,COLUMN_NAME,COLUMN_OCCUPATION,COLUMN_CITY,COLUMN_FRIENDSINCE };

    private static final String DATABASE_NAME = "friend.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_FRIENDS + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_NAME + " TEXT not null, "
            + COLUMN_OCCUPATION + " TEXT not null, "
            + COLUMN_CITY + " TEXT not null, "
            + COLUMN_FRIENDSINCE + " INTEGER not null"
            + ");";

    public FriendDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(FriendDatabaseHelper.class.getName(), "Upgrading Database from version " + oldVersion + " to " + newVersion);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FRIENDS);
        onCreate(db);
    }

    public void addFriend(FriendData friend) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, friend.getName());
        values.put(COLUMN_OCCUPATION, friend.getOccupation());
        values.put(COLUMN_CITY, friend.getCity());
        values.put(COLUMN_FRIENDSINCE, friend.getFriendSince());

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_FRIENDS, null, values);

        db.close();

    }

    public Cursor getFriend(String name){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_FRIENDS,
                COLUMNS,
                "name = ?",
                new String[] {String.valueOf(name)},
                null,
                null,
                null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        else {
            return null;
        }
        return cursor;
    }

    public Cursor getAllFriends() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_FRIENDS, null);


        return cursor;
    }

    public void deleteFriend(String name){
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_FRIENDS,
                COLUMN_NAME + " = ?",
                new String[] {String.valueOf((name))});

        db.close();
    }
}
