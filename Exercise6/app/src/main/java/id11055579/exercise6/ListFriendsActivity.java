package id11055579.exercise6;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import id11055579.exercise6.models.FriendData;

/**
 * Created by David on 8/09/2015.
 */
public class ListFriendsActivity extends AppCompatActivity {


    private FriendAdaptor mFriendAdaptor;
    private ListView mFriendListView;

    private FriendDatabaseHelper mFriendsDb;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_friends);
        mFriendsDb = new FriendDatabaseHelper(this);
        mFriendListView = (ListView) findViewById(R.id.friend_list_view);

        Intent intent = this.getIntent(); //check how this was called
        if (intent != null){
            String friendToFind = intent.getExtras().getString(Constants.FRIEND_TO_FIND);
            if (friendToFind.equals(Constants.ALL)) {       //show all friends
                mFriendAdaptor = new FriendAdaptor(this, mFriendsDb.getAllFriends(),0);
                mFriendListView.setAdapter(mFriendAdaptor);
            }
            else if (friendToFind == null){ //error case - ignore
                mFriendAdaptor = new FriendAdaptor(this, mFriendsDb.getAllFriends(),0);
                mFriendListView.setAdapter(mFriendAdaptor);
            }
            else {      //find one friend
                Cursor cursor = mFriendsDb.getFriend(friendToFind);
                if (cursor.getCount() !=0){     //if friend found
                    mFriendAdaptor = new FriendAdaptor(this, cursor,0);
                    mFriendListView.setAdapter(mFriendAdaptor);
                }
                else {      //none found
                    Toast.makeText(getApplicationContext(), R.string.friend_not_found, Toast.LENGTH_SHORT).show();
                    mFriendAdaptor = new FriendAdaptor(this, mFriendsDb.getAllFriends(),0);
                    mFriendListView.setAdapter(mFriendAdaptor);
                }
            }
        }
    }

    /**
     * listen to the delete buttons in the listview
     * @param v -the view that has been clicked
     */
    public void listDeleteClickHandler(View v){

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (preferences.getBoolean(getString(R.string.can_delete_key), false)) { //can I delete?
            String name = ((TextView) (((RelativeLayout) v.getParent()).getChildAt(0))).getText().toString();   //get the name from the clicked item
            Toast.makeText(getApplicationContext(), getString(R.string.deleting) + name + getString(R.string.from_database), Toast.LENGTH_SHORT).show();

            mFriendsDb.deleteFriend(name); //delete the item
            mFriendAdaptor.swapCursor(mFriendsDb.getAllFriends()); //refresh the listview
        }
        else //cant delete
        {
            Toast.makeText(getApplicationContext(), R.string.cnat_delete_toast, Toast.LENGTH_SHORT).show();
        }
    }


}
