package id11055579.exercise5;

import java.util.Random;

/**
 * Created by David Lee on 25/08/2015.
 */
public class TrainData {

    private String mPlatform;
    private int mArrivalTime;
    private String mStatus;
    private String mDestination;
    private String mDestinationTime;
    private int mRandomArrivalTime;


    public int getRandomArrivalTime() {
        return mRandomArrivalTime;
    }

    public void setRandomArrivalTime(int RandomArrivalTime) {
        this.mRandomArrivalTime = RandomArrivalTime;
    }

    public void refreshRandomArrivalTime()
    {
        mRandomArrivalTime = new Random().nextInt(19)+ 1;
    }

    public TrainData(int arrivalTime, String platform, String destinationTime, String destination, String status) {
        this.mArrivalTime = arrivalTime;
        this.mPlatform = platform;
        this.mDestinationTime = destinationTime;
        this.mDestination = destination;
        this.mStatus = status;
        this.mRandomArrivalTime = new Random().nextInt(19) + 1;
    }

    public String getPlatform() {
        return mPlatform;
    }

    public void setPlatform(String mPlatform) {

        this.mPlatform = mPlatform;
    }

    public int getArrivalTime() {

        return mArrivalTime;
    }

    public void setArrivalTime(int mArrivalTime) {

        this.mArrivalTime = mArrivalTime;
    }

    public String getStatus() {

        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getDestination() {
        return mDestination;
    }

    public void setDestination(String mDestination) {
        this.mDestination = mDestination;
    }

    public String getDestinationTime() {

        return mDestinationTime;
    }

    public void setDestinationTime(String mDestinationTime) {
        this.mDestinationTime = mDestinationTime;
    }




}
