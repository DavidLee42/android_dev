package id11055579.exercise5;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TrainData> mTrainList;
    private TrainAdaptor mTrainAdaptor;
    private ListView mTrainListView;
    private ProgressDialog mRefreshDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        populateTrainList();

        mTrainAdaptor = new TrainAdaptor(this, mTrainList);
        mTrainListView = (ListView) findViewById(R.id.listView);
        mTrainListView.setAdapter(mTrainAdaptor);

        mTrainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //if (view.getId() == R.id.left_linear_layout) {
                    mTrainList.get(position).refreshRandomArrivalTime();
                    ((BaseAdapter) mTrainListView.getAdapter()).notifyDataSetChanged();
                    //new RefreshItemAsyncTask(position).execute();
                //}

            }
        });
    }

    private void populateTrainList(){
        mTrainList = new ArrayList<>();
        mTrainList.add(new TrainData(1414, "Albion Park Platform 1", "14:14", "Allawah", "On Time"));
        mTrainList.add(new TrainData(1312, "Faulconbridge Platform 2", "14:18", "Central", "Late"));
        mTrainList.add(new TrainData(1332, "Arncliffe Platform 2", "14:34", "Central", "On Time"));
        mTrainList.add(new TrainData(1455, "Penrith Platform 1", "14:40", "Katoomba", "On Time"));
        mTrainList.add(new TrainData(1434, "Epping Platform 5", "14:45", "Strathfield", "Late"));
        mTrainList.add(new TrainData(1452, "Berowra Platform 4", "14:56", "Beverly", "On Time"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {
            case R.id.menu_refresh:
                new RefreshAllTrainsAsyncTask(mTrainList).execute();
                return true;
            case R.id.menu_add:
                mTrainList.add(new TrainData(1452, "Central Station Platform 9 3/4", "14:56", "Hogwarts", "On Time"));
                ((BaseAdapter) mTrainListView.getAdapter()).notifyDataSetChanged();
                return true;
            case R.id.menu_delete_all:
                mTrainList.clear();
                ((BaseAdapter) mTrainListView.getAdapter()).notifyDataSetChanged();
                return true;
            case R.id.menu_quit:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private class RefreshAllTrainsAsyncTask extends AsyncTask<Void, Void, Void> {



        private ArrayList<TrainData> mTempTrainList;


        public RefreshAllTrainsAsyncTask(ArrayList<TrainData> tempTrainList) {
            mTempTrainList = tempTrainList;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mTrainListView.setVisibility(View.INVISIBLE);
            mRefreshDialog = ProgressDialog.show(MainActivity.this, "","" , true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try
            {
                for(TrainData data : mTempTrainList)
                data.refreshRandomArrivalTime();
                Thread.sleep(2000, 0);
            }
            catch(Exception ex)
            {
                //Log.e(Constants.TAG, getString(R.string.exception_string), ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mRefreshDialog.dismiss();
            mTrainList = mTempTrainList;
            ((BaseAdapter) mTrainListView.getAdapter()).notifyDataSetChanged();
            mTrainListView.setVisibility(View.VISIBLE);
        }


    }

    private class RefreshItemAsyncTask extends AsyncTask<Void, Void, Void> {



        private int mPosition;


        public RefreshItemAsyncTask(int position) {
            mPosition = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mRefreshDialog = ProgressDialog.show(MainActivity.this, "","" , true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try
            {

                Thread.sleep(2000, 0);
            }
            catch(Exception ex)
            {
                //Log.e(Constants.TAG, getString(R.string.exception_string), ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mRefreshDialog.dismiss();

            ((BaseAdapter) mTrainListView.getAdapter()).notifyDataSetChanged();
            mTrainListView.setVisibility(View.VISIBLE);
        }


    }


}
