package id11055579.exercise5;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by David Lee on 25/08/2015.
 */
public class TrainAdaptor extends BaseAdapter{

    private Context mContext;
    private ArrayList<TrainData> mTrainList;
    private LayoutInflater mInflater;
    private Random mRandomGenerator;

    public TrainAdaptor(Context context, ArrayList trainList) {
        mContext = context;
        mTrainList = trainList;
        mInflater = LayoutInflater.from(context);
        mRandomGenerator = new Random();
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder myHolder;
        if (convertView == null) {
            convertView = this.mInflater.inflate(R.layout.list_item, parent, false);
            convertView.findViewById(R.id.arrival_time_text_view);
            myHolder = new ViewHolder();

            myHolder.arrivalTimeRandom = (TextView) convertView.findViewById(R.id.arrival_time_text_view);
            myHolder.refresh = (ProgressBar) convertView.findViewById(R.id.refresh_progress_bar);
            myHolder.platform = (TextView) convertView.findViewById(R.id.platform_text_view);
            myHolder.arrivalTimeActual = (TextView) convertView.findViewById(R.id.arrivalTime_text_view);
            myHolder.status = (TextView) convertView.findViewById(R.id.status_text_view);
            myHolder.destinationTime = (TextView) convertView.findViewById(R.id.destination_time_text_view);
            myHolder.destination = (TextView) convertView.findViewById(R.id.destination_text_view);
            convertView.setTag(myHolder);
        }
        else {
            myHolder = (ViewHolder) convertView.getTag();
        }
            TrainData tempTrainData =mTrainList.get(position);
            myHolder.arrivalTimeRandom.setText("" + tempTrainData.getRandomArrivalTime() + " mins");
            myHolder.platform.setText(tempTrainData.getPlatform());
            myHolder.arrivalTimeActual.setText("" + tempTrainData.getArrivalTime());
            myHolder.status.setText(tempTrainData.getStatus());
            if (tempTrainData.getStatus()== "Late"){
                myHolder.status.setTextColor(Color.RED);
            }
        else{
                myHolder.status.setTextColor(Color.GREEN);
            }

            myHolder.destinationTime.setText(tempTrainData.getDestinationTime());
            myHolder.destination.setText(tempTrainData.getDestination());




        return convertView;
    }




    @Override
    public long getItemId(int position) {

        return (long)position;
    }

    @Override
    public TrainData getItem(int position) {

        return mTrainList.get(position);
    }

    @Override
    public int getCount() {
        return mTrainList.size();

    }

    @Override
    public boolean isEnabled(int position)
    {
        return true;
    }

    private class ViewHolder {
        TextView arrivalTimeRandom;
        ProgressBar refresh;
        TextView platform;
        TextView arrivalTimeActual;
        TextView status;
        TextView destinationTime;
        TextView destination;

    }
}
