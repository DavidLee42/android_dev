package id11055579.exercise2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class ActivityTwo extends ActionBarActivity implements View.OnClickListener {

    TextView nameTv;
    TextView phoneTv;
    TextView phoneTypeTv;
    TextView emailTv;

    Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        nameTv = (TextView) findViewById(R.id.ActivityTwo_Name_tV);
        phoneTv = (TextView) findViewById(R.id.ActivityTwo_phone_tV);
        phoneTypeTv = (TextView) findViewById(R.id.ActivityTwo_phoneType_tV);
        emailTv = (TextView) findViewById(R.id.ActivityTwo_email_tV);

        Intent intent = getIntent();

        nameTv.setText(intent.getStringExtra(Constants.NAME));
        phoneTv.setText(intent.getStringExtra(Constants.PHONE));
        phoneTypeTv.setText(intent.getStringExtra(Constants.PHONE_TYPE));
        emailTv.setText(intent.getStringExtra(Constants.EMAIL));

        submitButton = (Button) findViewById(R.id.ActivityTwo_btnSUBMIT);
        submitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        String message;
        boolean isChecked = ((CheckBox) findViewById(R.id.ActivityTwo_agree_checkbox)).isChecked();

        if (isChecked)
        {
            message = getString(R.string.retMessageAgree);
        }
        else
        {
            message = getString(R.string.retMessageDisagree);
        }

        Intent intentMessage = new Intent();
        intentMessage.putExtra(Constants.MESSAGE, message);
        setResult(RESULT_OK, intentMessage);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_two, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override protected void onStart() { super.onStart(); Log.i(Constants.TAG, "Activity Two onStart"); }
    @Override protected void onResume() { super.onResume(); Log.i(Constants.TAG, "Activity Two onResume"); }
    @Override protected void onPause() { super.onPause(); Log.i(Constants.TAG, "Activity Two onPause"); }
    @Override protected void onStop() { super.onStop(); Log.i(Constants.TAG, "Activity Two onStop"); }
    @Override protected void onRestart() { super.onRestart(); Log.i(Constants.TAG, "Activity Two onRestart"); }
    @Override protected void onDestroy() { super.onDestroy(); Log.i(Constants.TAG, "Activity Two onDestroy"); }
    @Override protected void onSaveInstanceState(Bundle outState) { super.onSaveInstanceState(outState); Log.i(Constants.TAG, "Activity Two onSaveInstanceState"); }
    @Override protected void onRestoreInstanceState(Bundle savedInstanceState) { super.onRestoreInstanceState(savedInstanceState); Log.i(Constants.TAG, "Activity Two onRestoreInstanceState"); }
}
