package id11055579.exercise2;

/**
 * Created by David Lee on 11/08/2015.
 */
public class Constants {

    public static final String NAME = "name";
    public static final String PHONE = "phone";
    public static final String EMAIL = "email";
    public static final String PHONE_TYPE = "phoneType";
    public static final String MESSAGE = "message";

    public static final String TAG = "StateChange";
}
