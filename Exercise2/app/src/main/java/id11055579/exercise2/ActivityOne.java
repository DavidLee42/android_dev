package id11055579.exercise2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ActivityOne extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);



        Button submitButton = (Button) findViewById(R.id.btnSUBMIT);


        submitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) { //submit button clicked


                Intent intent = new Intent(ActivityOne.this, ActivityTwo.class);
                intent.putExtra(Constants.NAME, ((EditText) findViewById(R.id.ActivityOne_Name_Et)).getText().toString());
                intent.putExtra(Constants.PHONE, ((EditText) findViewById(R.id.ActivityOne_email_Et)).getText().toString());
                intent.putExtra(Constants.EMAIL, ((Spinner) findViewById(R.id.ActivityOne_phoneType_Sp)).getSelectedItem().toString());
                intent.putExtra(Constants.PHONE_TYPE, ((EditText) findViewById(R.id.ActivityOne_phone_Et)).getText().toString());
                startActivityForResult(intent, 2);
            }
        });

        Button ClearAllButton = (Button) findViewById(R.id.btnCLearAll);


        ClearAllButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) { //Clear All button clicked
                ((EditText) findViewById(R.id.ActivityOne_Name_Et)).setText("");
                ((EditText) findViewById(R.id.ActivityOne_email_Et)).setText("");
                ((EditText) findViewById(R.id.ActivityOne_phone_Et)).setText("");
                ((Spinner) findViewById(R.id.ActivityOne_phoneType_Sp)).setSelection(0);

            }
        });

        Button ExitButton = (Button) findViewById(R.id.btnExit);


        ExitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) { //Exit button clicked
                finish();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2)
        {
            if(resultCode == RESULT_OK)
            {
                String message = data.getStringExtra(Constants.MESSAGE);
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(getApplicationContext(), R.string.ErrorString, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override protected void onStart() { super.onStart(); Log.i(Constants.TAG, "Activity One onStart"); }
    @Override protected void onResume() { super.onResume(); Log.i(Constants.TAG, "Activity One onResume"); }
    @Override protected void onPause() { super.onPause(); Log.i(Constants.TAG, "Activity One onPause"); }
    @Override protected void onStop() { super.onStop(); Log.i(Constants.TAG, "Activity One onStop"); }
    @Override protected void onRestart() { super.onRestart(); Log.i(Constants.TAG, "Activity One onRestart"); }
    @Override protected void onDestroy() { super.onDestroy(); Log.i(Constants.TAG, "Activity One onDestroy"); }
    @Override protected void onSaveInstanceState(Bundle outState) { super.onSaveInstanceState(outState); Log.i(Constants.TAG, "Activity One onSaveInstanceState"); }
    @Override protected void onRestoreInstanceState(Bundle savedInstanceState) { super.onRestoreInstanceState(savedInstanceState); Log.i(Constants.TAG, "Activity One onRestoreInstanceState"); }

}
