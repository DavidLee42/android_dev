/*
 * Copyright (C) 2015 David Lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package id11055579.exercise4;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * MAD Exercise 4
 * Implements progress dialogs, spinners, and asynchronous tasks per the assignment spec
 */
public class Activity_One extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private Spinner mFileSelectSpinner;
    private String mSelectedFileStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__one);

        mFileSelectSpinner = (Spinner) findViewById(R.id.spnFileSelect);
        mFileSelectSpinner.setOnItemSelectedListener(this);

        Button mDownloadFileButton = (Button) findViewById(R.id.btnDownloadSelected);
        mDownloadFileButton.setOnClickListener(this);

        Button mDownloadAllButton = (Button) findViewById(R.id.btnDownloadAll);
        mDownloadAllButton.setOnClickListener(this);

        Button mCalculateTriNumberButton = (Button) findViewById(R.id.btnCalculateTriNumber);
        mCalculateTriNumberButton.setOnClickListener(this);
    }

    /**
     * Displays a toast of the current selected Item
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        mSelectedFileStr = parent.getItemAtPosition(pos).toString();
        Toast.makeText(parent.getContext(), getString(R.string.selected_file_toast) + mSelectedFileStr, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //nothing selected so nothing to do
    }

    /**
     * Handles the buttons in a switch statement
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.btnDownloadSelected):
                new DownloadSelectedAsyncTask(mSelectedFileStr).execute();
                break;
            case (R.id.btnDownloadAll):
                String[] fileNames = getResources().getStringArray(R.array.SpinnerFileNames);
                new DownloadAllAsyncTask(mFileSelectSpinner.getAdapter().getCount()).execute(fileNames);
                break;
            case (R.id.btnCalculateTriNumber):
                new CalculateTriangularAsyncTask(Constants.NUMBER_OF_ITERATIONS).execute();
                break;
            default:
                Log.e(Constants.TAG, getString(R.string.unknown_error));
                break;
        }
    }

    /**
     * Displays a progress bar to simulate downloading the task selected on the spinner.
     * The filename of the file to download is passed in via the constructor
     */
    private class DownloadSelectedAsyncTask extends AsyncTask<Void, Void, Void> {

        private String mFileName;
        private ProgressDialog mDownloadProgress;

        public DownloadSelectedAsyncTask(String filename) {
            mFileName = filename;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDownloadProgress = ProgressDialog.show(Activity_One.this, getString(R.string.downloading_string),
                    mFileName, true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            try
            {
                Thread.sleep(Constants.SLEEP_TIME, 0);
            }
            catch(Exception ex)
            {
                Log.e(Constants.TAG, getString(R.string.exception_string), ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mDownloadProgress.dismiss();
        }
    }

    /**
     * Displays a progress bar to simulate downloading all the files listed in the array resource for the spinner.
     * the number of files is passed in via the constructor.
     * the array of filenames is passed in as a strung parameter
     */
    private class DownloadAllAsyncTask extends AsyncTask<String, String, Void> {


        private ProgressDialog mDownloadAllProgressDialog;
        private int mNumFiles;


        public DownloadAllAsyncTask(int numFiles) {
            mNumFiles = numFiles;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDownloadAllProgressDialog = new ProgressDialog(Activity_One.this);
            mDownloadAllProgressDialog.setTitle(getString(R.string.download_all_files));
            mDownloadAllProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mDownloadAllProgressDialog.setProgress(0);
            mDownloadAllProgressDialog.setMax(mNumFiles);
            mDownloadAllProgressDialog.setMessage(getString(R.string.downloading_string));
            mDownloadAllProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            mDownloadAllProgressDialog.setMessage(values[0]);
            mDownloadAllProgressDialog.incrementProgressBy(1);
            super.onProgressUpdate(values);
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                for (int i = 0; i < mNumFiles; i++)
                {
                    publishProgress(getString(R.string.downloading_string) + params[i]);
                    Thread.sleep(Constants.SLEEP_TIME, 0);
                }
            }
            catch(Exception ex)
            {
                Log.e(Constants.TAG, getString(R.string.exception_string), ex);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mDownloadAllProgressDialog.dismiss();
        }
    }

    /**
     * Displays a progress bar and calculates the triangular numbers, returning a Long containing the result
     * The iterations to count to are passed in via the constructor (a constant in this case)
     * The result is returned to onPostExecute and displayed in a toast
     */
    private class CalculateTriangularAsyncTask extends AsyncTask<Void, Void, Long> {

        private ProgressDialog mCalculateTriangularProgressDialog;
        private int mNumberOfIterations;

        /**
         * constructor: takes in the requested number of iterations
         */
        public CalculateTriangularAsyncTask(int numberOfIterations) {
            mNumberOfIterations = numberOfIterations;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mCalculateTriangularProgressDialog = new ProgressDialog(Activity_One.this);
            mCalculateTriangularProgressDialog.setTitle(getString(R.string.calculating_tri_numbers));
            mCalculateTriangularProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mCalculateTriangularProgressDialog.setProgress(0);
            mCalculateTriangularProgressDialog.setMax(mNumberOfIterations);
            mCalculateTriangularProgressDialog.setMessage("");
            mCalculateTriangularProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Void... values)
        {
            mCalculateTriangularProgressDialog.incrementProgressBy(Constants.UPDATE_FREQUENCY);
            super.onProgressUpdate(values);
        }

        @Override
        protected Long doInBackground(Void... params)
        {
            long result = 0;
            try {
                for (int i = 1; i <= mNumberOfIterations; i++)
                {
                    result += i;
                    if ((i % Constants.UPDATE_FREQUENCY) == 0)
                    {
                        publishProgress();
                    }
                }
            }
            catch(Exception ex)
            {
                Log.e(Constants.TAG, getString(R.string.exception_string), ex);
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long myResult) {
            super.onPostExecute(myResult);

            mCalculateTriangularProgressDialog.dismiss();
            Toast.makeText(Activity_One.this,String.format(getString(R.string.tri_number_return_toast),
                    Constants.NUMBER_OF_ITERATIONS, myResult), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__one, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
