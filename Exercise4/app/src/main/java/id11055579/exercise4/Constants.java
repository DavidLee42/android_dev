/*
 * Copyright (C) 2015 David Lee
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id11055579.exercise4;

/**
 * Holds the constants used in exercise 4
 * Created by David on 19/08/2015.
 */
public class Constants {
    public static final String TAG = "DebugInfo";
    public static final int NUMBER_OF_ITERATIONS = 1000000;
    public static final int UPDATE_FREQUENCY = 50;
    public static final int SLEEP_TIME = 2000;
}
