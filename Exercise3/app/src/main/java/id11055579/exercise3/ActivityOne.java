package id11055579.exercise3;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityOne extends AppCompatActivity implements View.OnFocusChangeListener, View.OnClickListener
{


    public static final String BIN_TYPE_KEY = "BIN_TYPE_KEY";
    EditText mNrEt;
    Button mResetQuantityBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        mNrEt = (EditText) findViewById(R.id.binNrEt);
        mNrEt.setOnFocusChangeListener(this);
        if(this.getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
        {


            mResetQuantityBtn = (Button) findViewById(R.id.btnResetQuantity);
            mResetQuantityBtn.setOnClickListener(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_one, menu);
        return true;
    }

    public void onFocusChange(View view, boolean focus)
    {
        if (focus)
            Toast.makeText(getApplicationContext(), "Bin Quantity HAS Focus", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "Bin Quantity LOST Focus", Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v)
    {
        ((EditText) findViewById(R.id.binNrEt)).setText("");
    }

    public void clearAll(View v)
    {
        try {
            Log.d(Constants.TAG, "Activity One onStart");

            ((EditText) findViewById(R.id.binTypeEt)).setText("");
            ((EditText) findViewById(R.id.binSizeEt)).setText("");
            //mNrEt = null;
            mNrEt.setText("");
        }
        catch(NullPointerException e)
        {
            Log.e(Constants.TAG, "Null pointer, sorry :)");
        }
        finally
        {
            Log.e(Constants.TAG, "unknown exception");
        }

    }

    public void rotateScreen(View v)
    {
        if(this.getResources().getConfiguration().orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        Log.d(Constants.TAG, "Saving Instance State");
        savedInstanceState.putString(BIN_TYPE_KEY, ((EditText) findViewById(R.id.binTypeEt)).getText().toString());
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState)
    {
        Log.d(Constants.TAG, "Restoring Instance State");
        super.onRestoreInstanceState(savedInstanceState);
        ((EditText)findViewById(R.id.binTypeEt)).setText(savedInstanceState.getString(BIN_TYPE_KEY));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
